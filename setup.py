from platform import python_version
from setuptools import find_packages, setup


setup(
    name='human_browser',
    packages=find_packages(),
    version='0.1.0',
    description='Human like browser controller with ability for saving scenarios',
    author='Anton Skrypnyk',
    python_requires='>3.6.0',
    install_requires=["selenium~=4.4.3", "numpy", "scipy"]
)
