from functools import lru_cache
from typing import Union

from selenium.webdriver.remote.webelement import WebElement

from selenium.webdriver.remote.webelement import WebElement

from .actions.a_action import AAction
from .actions.action_queue import ActionQueue
from .actions.arguments_action import to_args
from .actions.delay_action import DelayType, DelayAction
from .actions.find_element import WaitElementPresent
from .keyboard import VirtualKeyboard
from .mouse import VirtualMouse
from .position import Position


class Actor:

    def __init__(self, driver):
        self._driver = driver
        self._queue = ActionQueue()
        self._typer = VirtualKeyboard(self._queue)
        self._mouse = VirtualMouse(self._driver, self._queue)

    def get_scenario(self):
        return self._queue

    def reset(self):
        self._queue = ActionQueue()
        self._typer = VirtualKeyboard(self._queue)
        self._mouse = VirtualMouse(self._driver, self._queue)

    def find(self, element_pattern: Union[list, object]):
        if isinstance(element_pattern, list):
            pattern_iter = iter(element_pattern)
            result_action = None
            try:
                current_pattern = next(pattern_iter)
                result_action = self.find(current_pattern)
                while True:
                    result_action.find(next(pattern_iter))

            except StopIteration:
                return result_action
        else:
            return WaitElementPresent(to_args(self._driver, 1, element_pattern))

    def scroll_to(self, element):
        self._mouse.scroll_to_offset(VirtualMouse.get_position_action(element))

    def click(self, element):
        self._mouse.move_to(element)
        self._mouse.click(element)

    def type(self, element: AAction, string: str):
        if len(string) == 0:
            return

        self._mouse.move_to(element)
        self._mouse.click(element)

        if len(string) == 1:
            self._typer.type_once(element, string)
        else:
            self._typer.type(element, string)

    def run(self):
        self._queue()

    def wait(self, delay: DelayType):
        self._queue.schedule(DelayAction(delay))

    def move_to_offset(self, position: Position):
        self._mouse.move_to_offset(position)

    def click_position(self, position: Position):
        self._mouse.click_offset(position)

    def schedule_action(self, action: AAction):
        self._queue.schedule(action)

    @lru_cache(maxsize=None)
    def find_cached(self, query):
        return self.find(query)
