from functools import wraps

from .a_action import AAction
from .arguments_action import ArgumentsAction


class LambdaWrapper:

    @classmethod
    def wrap(cls: AAction, func):
        """
        Functions that schedule other actions should not be wrapped as lambda actions
        this can cause infinite loops or unexpected behaviour

        If AAction subtype is passed as argument, it will be called before main lambda call
        """

        @wraps(func)
        def wrapper(*args, **kwargs):
            return cls(func, ArgumentsAction(*args, **kwargs))

        return wrapper
