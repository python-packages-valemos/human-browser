from typing import Generic

from .a_action import AAction, T
from .a_cached_action import ACachedAction


class GetValueAction(Generic[T], AAction[T]):

    def __init__(self, value):
        super().__init__()
        self._value = value

    def run(self, *args, **kwargs) -> T:
        return self._value


class GetCachedValueAction(Generic[T], ACachedAction[T]):

    def __init__(self, value):
        super().__init__()
        self._value = value

    def run(self, *args, **kwargs) -> T:
        return self._value
