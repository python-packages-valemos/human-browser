import logging

from .a_cached_action import ACachedAction
from .arguments_action import to_args
from .i_element_container import IElementContainer


class ChainingFindElement(ACachedAction, IElementContainer):

    def find(self, query):
        return ChainingFindElement(to_args(self, query))

    def run(self, *args, **kwargs):
        logging.debug(f"chain search for {args[1]}")
        return args[0].find_element(args[1])
