from abc import abstractmethod


class IElementContainer:

    @abstractmethod
    def find(self, query):
        pass
