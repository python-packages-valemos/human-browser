from typing import Callable, Optional

from .a_action import AAction


class ActionQueue(AAction):

    def __init__(self, queue=None, input_action: Optional[Callable] = None):
        super().__init__(input_action)
        self._queue: list[AAction] = queue if queue is not None else []

    def reset(self):
        for action in self._queue:
            action.reset()

    def run(self, *args, **kwargs):
        last_out = None
        try:
            queue_iter = iter(self._queue)
            last_out = next(queue_iter)(*args, **kwargs)
            while True:
                last_out = next(queue_iter)()
        except StopIteration:
            return last_out

    def schedule(self, action: AAction):
        if not isinstance(action, Callable):
            raise ValueError("cannot schedule not callable action")

        self._queue.append(action)
