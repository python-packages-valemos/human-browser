import logging
from typing import Callable, Optional

from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from .a_cached_action import ACachedAction
from .arguments_action import to_args
from .chanining_find_element import ChainingFindElement
from .i_element_container import IElementContainer


class AWebDriverWaitAction(ACachedAction, IElementContainer):

    _condition: Callable

    def __init__(self, input_action: Optional[Callable] = None):
        super().__init__(input_action)

    def run(self, *args, **kwargs):
        logging.debug(f"searching for element by {args[2]}")
        return WebDriverWait(args[0], args[1]).until(self.__class__._condition(args[2]))

    def find(self, query):
        return ChainingFindElement(to_args(self, query))


class WaitElementVisible(AWebDriverWaitAction):
    _condition = EC.visibility_of_element_located


class WaitElementPresent(AWebDriverWaitAction):
    _condition = EC.presence_of_element_located
