from abc import ABC
from typing import Generic, Optional, Callable

from .a_action import AAction, T


class ACachedAction(ABC, Generic[T], AAction[T]):

    """Runs only once and stores its results for later use"""

    def __init__(self, input_action: Optional[Callable] = None):
        super().__init__(input_action)
        self._is_ready = False
        self._output = None

    def __call__(self, *args, **kwargs) -> T:
        """Caches run results for later use"""
        if not self._is_ready:
            self._output = super().__call__(*args, **kwargs)
            self._is_ready = True

        return self._output

    def reset(self):
        self._is_ready = False
