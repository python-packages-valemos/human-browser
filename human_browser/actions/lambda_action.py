from functools import wraps

from .a_action import AAction
from .lambda_wrapper import LambdaWrapper


class LambdaAction(AAction, LambdaWrapper):

    def __init__(self, functor, input_action=None):
        super().__init__(input_action)
        self._callable = functor

    def run(self, *args, **kwargs):
        return self._callable(*args, **kwargs)
