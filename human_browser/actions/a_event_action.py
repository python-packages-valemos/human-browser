import logging
from abc import abstractmethod
from typing import Optional, Callable

from .a_action import AAction, T


class AEventAction(AAction):
    """
    Holds value that can be updated by event
    """

    def __init__(self, input_action: Optional[Callable] = None):
        super().__init__(input_action)
        self._callbacks = []

    @abstractmethod
    def run(self, *args, **kwargs) -> T:
        pass

    def add_callback(self, event_callback):
        self._callbacks.append(event_callback)

    def remove_observed(self, event_callback):
        try:
            self._callbacks.remove(event_callback)
        except ValueError:
            logging.error("no such callback observing values")

    def notify(self, *args, **kwargs):
        for callback in self._callbacks:
            callback(*args, **kwargs)
