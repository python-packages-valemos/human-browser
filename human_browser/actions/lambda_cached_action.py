from .a_cached_action import ACachedAction
from .lambda_wrapper import LambdaWrapper


class LambdaCachedAction(ACachedAction, LambdaWrapper):

    def __init__(self, functor, input_action=None):
        super().__init__(input_action)
        self._callable = functor
        self._reset_handler = None

    def run(self, *args, **kwargs):
        return self._callable(*args, **kwargs)
