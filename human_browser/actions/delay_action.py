import enum
import time

from .lambda_action import LambdaAction
from ..random import *


class DelayType(enum.Enum):
    KEYBOARD_TYPE = keyboard_type_delay
    REACTION = reaction_delay
    LONG_REACTION = long_reaction_delay
    THINKING = thinking_delay


class DelayAction(LambdaAction):

    def __init__(self, delay: DelayType, input_action=None):
        super().__init__(delay, input_action)

    def run(self, *args, **kwargs):
        time.sleep(super().run(*args, **kwargs))
