from abc import abstractmethod
from typing import TypeVar, Generic, Optional, Callable

T = TypeVar('T')


class AAction(Generic[T]):

    def __init__(self, input_action: Optional[Callable] = None):
        self._input_action = input_action

    def set_input(self, input_action):
        self._input_action = input_action

    @abstractmethod
    def run(self, *args, **kwargs) -> T:
        pass

    def reset(self):
        pass

    def unwrap(self):
        """verbose delegate for call without parameters"""
        return self.__call__()

    def __call__(self, *args, **kwargs) -> T:
        if self._input_action is not None:
            ret = self._input_action()
            args, kwargs = AAction._pack_arguments(ret)

        args, kwargs = AAction._unwrap_arguments(args, kwargs)

        return self.run(*args, **kwargs)

    @staticmethod
    def _pack_arguments(ret):
        args, kwargs = ret if len(ret) == 2 else (ret, {})
        if not isinstance(args, tuple): args = (args,)
        return args, kwargs

    @staticmethod
    def _unwrap_arguments(args: tuple, kwargs: dict):
        return [AAction.unwrap(arg) for arg in args], \
               {key: AAction.unwrap(arg) for key, arg in kwargs.items()}

    @staticmethod
    def unwrap(obj):
        return obj() if isinstance(obj, AAction) else obj
