from .a_action import AAction


class ArgumentsAction(AAction):

    def __init__(self, *args, **kwargs):
        super().__init__(None)
        self.arguments = args, kwargs

    def run(self, *args, **kwargs):
        return self.arguments


def to_args(*args, **kwargs):
    return ArgumentsAction(*args, **kwargs)
