from dataclasses import dataclass


@dataclass
class Position:
    x: int
    y: int

    @classmethod
    def from_dict(cls, position_dict):
        return Position(position_dict['x'], position_dict['y'])
