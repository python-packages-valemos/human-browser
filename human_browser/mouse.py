import numpy as np
import scipy.interpolate as si
from selenium.webdriver import ActionChains
from selenium.webdriver.remote.webelement import WebElement

from .actions.a_action import AAction
from .actions.action_queue import ActionQueue
from .actions.lambda_action import LambdaAction
from .actions.lambda_cached_action import LambdaCachedAction
from .position import Position
from .random import random_human_value
from .rectangle import Rectangle


class VirtualMouse:

    def __init__(self, driver, queue: ActionQueue) -> None:
        self._queue = queue
        self._driver = driver
        self._action_chains = self._get_action_chains(driver)
        self._last_position = None

    def _move_between_action(self, start: Position, end: Position):
        return ActionQueue([
            self._move_offset_action(start),
            self._move_offset_action(end),
        ])

    def move_by_curve(self, curve):
        for mouse_x, mouse_y in self.interpolate_curve(curve):
            self.move_to_offset(Position(mouse_x, mouse_y))

    @staticmethod
    def generate_anchors(start, end):
        # todo: create path
        return []

    @staticmethod
    def interpolate_curve(points):
        points = np.array(points)

        x = points[:, 0]
        y = points[:, 1]

        t = range(len(points))
        ipl_t = np.linspace(0.0, len(points) - 1, 100)

        x_tup = si.splrep(t, x, k=3)
        y_tup = si.splrep(t, y, k=3)

        x_list = list(x_tup)
        xl = x.tolist()
        x_list[1] = xl + [0.0, 0.0, 0.0, 0.0]

        y_list = list(y_tup)
        yl = y.tolist()
        y_list[1] = yl + [0.0, 0.0, 0.0, 0.0]

        yield from zip(si.splev(ipl_t, x_list), si.splev(ipl_t, y_list))

    def click(self, element: AAction[WebElement]):
        self._queue.schedule(self._click_element_action(element))

    def click_offset(self, offset: Position):
        self.move_to_offset(offset)
        self._queue.schedule(self._click_offset_action(offset))

    @staticmethod
    def get_click_position(element: WebElement):
        rect = Rectangle.from_dict(element.rect)

        x = random_human_value(rect.x_min * 0.90, rect.x_max * 0.90)
        y = random_human_value(rect.y_min * 0.90, rect.y_max * 0.90)

        return Position(x, y)

    def move_to(self, element):
        self._queue.schedule(self._move_to_element_action(element))

    def scroll_to_offset(self, offset):
        self._queue.schedule(self._scroll_to_offset(offset))

    def move_to_offset(self, position: Position):
        if self._last_position == position:
            return

        if self._last_position is None:
            self._queue.schedule(self._move_offset_action(position))
        else:
            self._queue.schedule(self._move_between_action(self._last_position, position))
            self._last_position = position

    @LambdaAction.wrap
    def _scroll_to_offset(self, offset: Position):
        scroll_by_coord = f'window.scrollTo({offset.x},{offset.y});'
        AAction.unwrap(self._driver).execute_script(scroll_by_coord)

    @staticmethod
    @LambdaAction.wrap
    def _click_element_action(element):
        element.click()

    @staticmethod
    @LambdaCachedAction.wrap
    def _get_action_chains(driver):
        return ActionChains(driver)

    @LambdaAction.wrap
    def _click_offset_action(self, offset: Position):
        # TODO finish click
        AAction.unwrap(self._action_chains).move_by_offset(offset.x, offset.y)

    @LambdaAction.wrap
    def _move_offset_action(self, position: Position):
        chains = AAction.unwrap(self._action_chains)
        chains.move_by_offset(position.x, position.y)
        chains.perform()

    @LambdaAction.wrap
    def _move_to_element_action(self, element):
        chains = AAction.unwrap(self._action_chains)
        chains.move_to_element(element)
        chains.perform()

    @staticmethod
    @LambdaAction.wrap
    def get_position_action(element):
        return Position.from_dict(element.location)
