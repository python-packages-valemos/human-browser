from selenium.webdriver.remote.webelement import WebElement

from .actions.action_queue import ActionQueue
from .actions.delay_action import DelayType, DelayAction
from .actions.lambda_action import LambdaAction


class VirtualKeyboard:

    def __init__(self, queue: ActionQueue) -> None:
        self._queue = queue

    def type(self, element, string: str):
        self._queue.schedule(ActionQueue([self._type_once_action(element, char) for char in string]))

    def type_once(self, element, character: str):
        self._queue.schedule(self._type_once_action(element, character))

    def _type_once_action(self, element, character: str):
        if character.isalpha() or character.isnumeric():
            delay = DelayType.KEYBOARD_TYPE
        else:
            delay = DelayType.REACTION

        return ActionQueue([
            self._send_keys_action(element, character),
            DelayAction(delay)
        ])

    @staticmethod
    @LambdaAction.wrap
    def _send_keys_action(element, keys: str):
        element.send_keys(keys)
