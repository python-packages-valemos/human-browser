from dataclasses import dataclass

from .position import Position


@dataclass
class Rectangle:
    position: Position
    width: int
    height: int

    @classmethod
    def from_dict(cls, rect_dict):
        return cls(Position.from_dict(rect_dict), rect_dict['width'], rect_dict['height'])

    @property
    def x_min(self):
        return self.position.x

    @property
    def y_min(self):
        return self.position.y

    @property
    def x_max(self):
        return self.position.x

    @property
    def y_max(self):
        return self.position.y
