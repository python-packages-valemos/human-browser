import random


def clamp(value, min_v, max_v):
    return max(min(value, max_v), min_v)


def random_deviation(min_v, amplitude):
    a = min_v
    b = min_v + amplitude
    if a > b: a, b = b, a
    return random.uniform(a, b)


def random_human_value(minimum, maximum):
    interval_magnitude = maximum - minimum
    mu = interval_magnitude * 0.5 + minimum
    sigma = interval_magnitude / 4
    value = random.normalvariate(mu, sigma)

    min_deviation = random_deviation(0, interval_magnitude * 0.1)
    max_deviation = random_deviation(0, interval_magnitude * 0.1)

    value = clamp(value, minimum + min_deviation, maximum - max_deviation)

    return value


def reaction_delay():
    return random_human_value(0.13, 0.35)


def long_reaction_delay():
    return random_human_value(0.6, 1.3)


def thinking_delay():
    return random_human_value(4, 10)


def keyboard_type_delay():
    return random_human_value(0.01, 0.2)


if __name__ == '__main__':
    import matplotlib.pyplot as plt

    plt.hist([keyboard_type_delay() for i in range(10000)])
    plt.show()
